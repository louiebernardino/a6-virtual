//Declare dependencies and model
const Task = require("../models/tasks");
const express = require("express");
const router = express.Router(); //to handle routing
const auth = require("../middleware/auth")

//1) CREATE
router.post("/", auth, async (req, res) => {
	const task = new Task({
		//use SPREAD OPERATOR to copy the body
		...req.body, 
		memberId: req.member._id
	})
	//Commented out for async operation//
	// task.save()
	// 	.then(() => {
	// 		res.send(task)
	// 	})
	// 	.catch((e) => {
	// 		res.status(400).send(e)
	// 	})
	//Commented out for async operation//
	try {
		await task.save(); 
		res.status(201).send(task)
	} catch(e) {
		res.status(400).send(e)
	}
})

//2) GET ALL TASKS OF LOGGED IN MEMBER
router.get("/", auth, async (req, res) => {
	//Commented out for async operation//
	// Task.find()
	// 	.then((tasks) => {
	// 		return res.status(200).send(tasks)
	// 	})
	// 	.catch((e) => {
	// 		return res.status(500).send(e)
	// 	})
	//Commented out for async operation//
	try {
		//approach 1
		// const tasks = await Task.find({memberId: req.member._id})
		//approach 2
		await req.member.populate("tasks").execPopulate()


		res.send(req.member.tasks)
	} catch(e) {
		return res.status(404).send(e)
	}
});

//3) GET A TASK OF LOGGED IN MEMBER
router.get("/:id", auth,  async (req, res) => {
	const _id = req.params.id;
	//Commented out for async operation//
	// Task.findById(_id)
	// 	.then((task) => {
	// 		if(!task) {
	// 			return res.status(404).send(e)
	// 		}

	// 		return res.send(task)
	// 	})
	// 	.catch((e) => {
	// 		return res.status(500).send(e)
	// 	})
	//Commented out for async operation//	
	try {
		const task = await Task.findById(_id);
		if(!task) {
			return res.status(404).send("Task doesn't exist!");
		}
		res.send(task)
	} catch(e) {
		return res.status(500).send(e);
	}
});

//4) UPDATE A TASK - OWNER OF THE TASK CAN UPDATE
router.patch("/:id", auth, async (req, res) => {
	const _id = req.params.id;
	//Commented out for async operation//
	// Task.findByIdAndUpdate(_id, req.body, { new: true })
	// 	.then((task) => {
	// 		if(!task) {
	// 			return res.status(404).send(e)
	// 		}

	// 		return res.send(task)
	// 	})
	// 	.catch((e) => {
	// 		return res.status(500).send(e)
	// 	})
	//Commented out for async operation//
	try {
		const task = await Task.findByIdAndUpdate(_id, req.body, { new: true });
		if(!task) {
			return res.status(404).send("Task doesn't exist!");
		}
		res.send(task);
	} catch(e) {
		return res.status(500).send(e);
	}
});

//5) DELETE A TASK - OWNER OF THE TASK CAN UPDATE
router.delete("/:id", auth, async (req, res) => {
	const _id = req.params.id;
	//Commented out for async operation//
	// Task.findByIdAndDelete(_id)
	// 	.then((task) => {
	// 		if(!task) {
	// 			return res.status(404).send(e)
	// 		}

	// 		return res.send(task)
	// 	})
	// 	.catch((e) => {
	// 		return res.status(500).send(e)
	// 	})
	//Commented out for async operation//	
	try {
		const task = await Task.findByIdAndDelete(_id);
		if(!task) {
			return res.status(404).send("Task doesn't exist");
		}
		res.send(task);
	} catch(e) {
		return res.status(500).send(e);
	}
});

module.exports = router;

//GET A TASK OF THE PERSON LOGGED IN

//UPDATE A TASK 

//DELETE A TASK

//HM - APPLY VIRTUAL REL TO MEMBER-TEAM so that you can get all members of a team using populate